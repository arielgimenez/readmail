/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import obj.Mensaje;
import util.MsjCod;

/**
 *
 * @author Ariel
 */
public class MensajeController {

    private Conexion conn;
    private static final String INSERT_MSJ = "INSERT INTO mensajes(id,fecha,froms,subject) VALUES(?,?,?,?)";
    private static final String SELECT_MSJ = "SELECT * FROM mensajes ORDER BY id DESC";
    private static final String DELETE_MSJ = "DELETE FROM mensajes";
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public int guardarMsjs(ArrayList<Mensaje> mensajes) {
        clearMsj();
        conn = Conexion.saberEstado();
        PreparedStatement ps = null;
        int countMsj = 0;
        try {
            conn.getConexion().setAutoCommit(false);
            if (!mensajes.isEmpty()) {
                for (Mensaje mensaje : mensajes) {
                    ps = conn.getConexion().prepareStatement(INSERT_MSJ);
                    ps.setInt(1, mensaje.getId());
                    ps.setString(2, FORMAT.format(mensaje.getFecha()));
                    ps.setString(3, mensaje.getFrom());
                    ps.setString(4, mensaje.getSubject());                   

                    if (ps.executeUpdate() > 0) {
                        countMsj++;
                    }
                }
                if(mensajes.size() == countMsj){
                    conn.getConexion().commit();
                    return MsjCod.OK;
                } else {
                    conn.getConexion().rollback();
                }
            } else {
                return MsjCod.SIN_REG_SAVE;
            }

        } catch (SQLException ex) {
            try {
                conn.getConexion().rollback();
                Logger.getLogger(MensajeController.class.getName()).log(Level.SEVERE, null, ex);
                return MsjCod.ERROR_SAVE;
            } catch (SQLException ex1) {
                Logger.getLogger(MensajeController.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            conn.cerrarConexion();
        }
        return MsjCod.ERROR_SAVE;
    }
    
    public ArrayList<Mensaje> extraerMsjs(){
        ArrayList<Mensaje> mensajes = new ArrayList<>();
        conn = Conexion.saberEstado();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.getConexion().prepareStatement(SELECT_MSJ);
            rs = ps.executeQuery();

            while (rs.next()) {
                Mensaje mensaje = new Mensaje();
                mensaje.setId(rs.getInt("id"));
                mensaje.setFecha(FORMAT.parse(rs.getString("fecha")));
                mensaje.setFrom(rs.getString("froms"));
                mensaje.setSubject(rs.getString("subject"));
                mensajes.add(mensaje);
            }
        } catch (SQLException | ParseException ex) {
            Logger.getLogger(MensajeController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            conn.cerrarConexion();
        }
        return mensajes;
    }
    
    private void clearMsj(){
        conn = Conexion.saberEstado();
        PreparedStatement ps = null;
        try {
            ps = conn.getConexion().prepareStatement(DELETE_MSJ);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(MensajeController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            conn.cerrarConexion();
        }
    }
}
