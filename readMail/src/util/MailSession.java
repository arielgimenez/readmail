package util;

import conexion.Conexion;
import controller.MensajeController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import obj.Mensaje;
import view.Cargando;
import view.Vista;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ariel
 */
public class MailSession implements Runnable {

    boolean done;
    private String mail;
    private String password;
    private Properties props = System.getProperties();
    private Session sesion;
    private Store store;
    private Folder folder;
    private Message[] mensajes;
    private JTable tabla;
    private Cargando cargando;
    private MensajeController mensajeController;
    Vista vista;

    public MailSession(String mail, String password, JTable tabla, Vista vista) {
        this.vista = vista;
        this.mail = mail;
        this.password = password;
        this.tabla = tabla;
        done = false;
        mensajeController = new MensajeController();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Properties getProps() {
        return props;
    }

    public void setProps(Properties props) {
        this.props = props;
    }

    public Session getSesion() {
        return sesion;
    }

    public void setSesion(Session sesion) {
        this.sesion = sesion;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    public Message[] getMensajes() {
        return mensajes;
    }

    public void setMensajes(Message[] mensajes) {
        this.mensajes = mensajes;
    }

    public JTable getTabla() {
        return tabla;
    }

    public void setTabla(JTable tabla) {
        this.tabla = tabla;
    }

    @Override
    public void run() {
        while (!done) {
            inHabilitarVista();
            conectar();
        }
    }

    public void conectar() {
        cargarPopiedades();
        cargarSession();
        if (!mail.isEmpty() || !password.isEmpty()) {
            obtenerStoreFolder();
            cargarTabla(obtenerMsj());
            stopThread();
        } else {
            habilitarVista();
            JOptionPane.showMessageDialog(null, MsjCod.ERROR_DATOS_TEXT);
        }
    }

    public void cargarPopiedades() {
        props.setProperty("mail.pop3.starttls.enable", "false");
        props.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.pop3.socketFactory.fallback", "false");
        props.setProperty("mail.pop3.port", "995");
        props.setProperty("mail.pop3.socketFactory.port", "995");
    }

    public void cargarSession() {
        sesion = Session.getInstance(props);
        sesion.setDebug(false);
    }

    public void obtenerStoreFolder() {
        try {
            store = sesion.getStore("pop3");
            store.connect("pop.gmail.com", mail, password);
            folder = store.getFolder("INBOX");
            folder.open(Folder.READ_ONLY);
        } catch (MessagingException ex) {
            habilitarVista();
            Logger.getLogger(MailSession.class.getName()).log(Level.SEVERE, null, ex);
            String msg = ex.getMessage();
            switch (msg) {
                case MsjCod.ERROR_FIREWALL:
                    JOptionPane.showMessageDialog(null, MsjCod.ERROR_FIREWALL_TEXT);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, MsjCod.ERROR_GMAIL_TEXT);
                    break;
            }

        }
    }

    public ArrayList<Mensaje> obtenerMsj() {
        ArrayList<Mensaje> mensajesList = new ArrayList<>();
        try {
            mensajes = folder.getMessages();
            if (mensajes.length > 0) {
                for (Message mensaje : mensajes) {

                    Mensaje m = new Mensaje();
                    m.setId(mensaje.getMessageNumber());
                    m.setFecha(mensaje.getSentDate());
                    InternetAddress address = (InternetAddress) mensaje.getFrom()[0];
                    m.setFrom(address.getAddress());
                    m.setSubject(mensaje.getSubject());

                    if (m.getSubject().contains("DevOps") || interpretacionContenido(mensaje).contains("DevOps")) {
                        mensajesList.add(m);
                    }
                }
            }
        } catch (MessagingException ex) {
            habilitarVista();
            Logger.getLogger(MailSession.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mensajesList;
    }

    public void stopThread() {
        this.done = true;
        habilitarVista();
    }

    private void habilitarVista() {
        this.vista.setEnabled(true);
        this.cargando.dispose();
    }

    private void inHabilitarVista() {
        this.vista.setEnabled(false);
        cargando = new Cargando();
        cargando.setVisible(true);
        cargando.setAlwaysOnTop(true);
    }

    private String interpretacionContenido(Message mensaje) {

        String contenidoMsj = "";
        try {
            if (mensaje.isMimeType("text/*")) {
                contenidoMsj = (String) mensaje.getContent();
            }

            if (mensaje.isMimeType("multipart/*")) {
                contenidoMsj = msjMultiparte(mensaje);
            }

        } catch (MessagingException ex) {
            Logger.getLogger(MailSession.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MailSession.class.getName()).log(Level.SEVERE, null, ex);
        }
        return contenidoMsj;
    }

    private String msjMultiparte(Message mensaje) {

        String contenidoMsj = "";
        try {

            Multipart multi;
            multi = (Multipart) mensaje.getContent();

            // Extraemos cada una de las partes.
            for (int j = 0; j < multi.getCount(); j++) {
                Part unaParte = multi.getBodyPart(j);
                // Volvemos a analizar cada parte de la MultiParte
                if (unaParte.isMimeType("text/*")) {
                    contenidoMsj = contenidoMsj + "\n" + unaParte.getContent();
                } else if (unaParte.isMimeType("multipart/*")) {
                    contenidoMsj = contenidoMsj + "\n" + msjMultiparte((Message) unaParte);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(MailSession.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(MailSession.class.getName()).log(Level.SEVERE, null, ex);
        }

        return contenidoMsj;
    }

    private void cargarTabla(ArrayList<Mensaje> mensajes) {
        GrillaMsj grillaMsj;
        switch (mensajeController.guardarMsjs(mensajes)) {
            case MsjCod.SIN_REG_SAVE:
                grillaMsj = new GrillaMsj(mensajeController.extraerMsjs());
                vista.getjTableMsj().setModel(grillaMsj);
                habilitarVista();
                JOptionPane.showMessageDialog(null, MsjCod.SIN_REG_SAVE_TEXT);
                break;
            case (MsjCod.ERROR_SAVE):
                grillaMsj = new GrillaMsj(mensajeController.extraerMsjs());
                vista.getjTableMsj().setModel(grillaMsj);
                habilitarVista();
                JOptionPane.showMessageDialog(null, MsjCod.ERROR_SAVE_TEXT);
                break;
            case (MsjCod.OK):
                grillaMsj = new GrillaMsj(mensajeController.extraerMsjs());
                vista.getjTableMsj().setModel(grillaMsj);
                habilitarVista();
                JOptionPane.showMessageDialog(null, MsjCod.OK_TEXT);
                break;
            }    
        }
}
