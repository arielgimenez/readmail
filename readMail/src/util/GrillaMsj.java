/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import controller.MensajeController;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.table.AbstractTableModel;
import obj.Mensaje;

/**
 *
 * @author Ariel
 */
public class GrillaMsj extends AbstractTableModel{
    
    ArrayList<Mensaje> tareas;
    public boolean cellEditable = false;
    public int[] columnEditable=null;
    public ArrayList <String> columnIdentifiers;
    String [] columNames;
    MensajeController acc;
    ArrayList header;
    SimpleDateFormat format;
    
    public ArrayList<Mensaje> getTareas() {
        return tareas;
    }

    public void setTareas(ArrayList<Mensaje> tareas) {
        this.tareas = tareas;
    }

    public GrillaMsj(ArrayList<Mensaje> empleados) {
        format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        setTareas(empleados);
        
        columNames=new String[4];
        columNames[0]="Numero Msj";
        columNames[1]="Fecha";
        columNames[2]="From";
        columNames[3]="Subject";
        
        
        header = new ArrayList();
        header.addAll(Arrays.asList(columNames));
        this.columnIdentifiers=header;
        
    }
    
            
    
    @Override
    public int getRowCount() {
        return tareas.size();
    }
    @Override
    public int getColumnCount() {
        return columnIdentifiers.size();
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        Mensaje misDatos = (Mensaje) tareas.get(rowIndex);
        switch(columnIndex){
            case 0:
                return misDatos.getId();
            case 1:
                return format.format(misDatos.getFecha());
            case 2:
                return misDatos.getFrom();
            case 3:
                return misDatos.getSubject();    
            default:
                return null;
        }
          
    }
    @Override
    public String getColumnName(int column) {
        return columnIdentifiers.get(column);
    }
    public ArrayList getColumnIdentifiers() {
        return columnIdentifiers;
    }
    public void removeRow(int row) {
        tareas.remove(row);
        fireTableRowsDeleted(row, row);
    }
    public void addRow(Mensaje data){
        this.tareas.add(data);
        fireTableDataChanged();
    }
    public Mensaje getRow(int index){
        return tareas.get(index);
    }
}
