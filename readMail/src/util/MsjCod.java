/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Ariel
 */
public class MsjCod {
    public static final int OK = 1;
    public static final String OK_TEXT = "Se actualizo la BD de msj";
    public static final int ERROR_SAVE = 2;
    public static final String ERROR_SAVE_TEXT = "Ocurrio un error al guardar intente nuevamente";
    public static final int SIN_REG_SAVE = 3;
    public static final String SIN_REG_SAVE_TEXT = "No hay msj nuevos";
    public static final int ERROR_READ = 4;
    public static final String ERROR_READ_TEXT = "No se pudo leer la BD";
    public static final int ERROR_BD = 5;
    public static final String ERROR_BD_TEXT = "No se pudo conectar con la BD";
    public static final String ERROR_FIREWALL = "Connect failed";
    public static final String ERROR_FIREWALL_TEXT = "Error al conectar, esto puede suceder por:\n 1.Algun Firewall esta bloqueando";
    public static final int ERROR_GMAIL = 6;
    public static final String ERROR_GMAIL_TEXT = "Error al conectar, esto puede suceder porque en su cuenta de Gmail:\n 1.La verificación en dos pasos esta activada."
                            + "\n 2.No se permite aplicación menos segura (debe estar activada)."
                            + "\n 3.Usuario y/o contraseña incorrectos.";
    public static final String ERROR_DATOS_TEXT = "Para poder conectar debe ingresar mail y contraseña";
}
